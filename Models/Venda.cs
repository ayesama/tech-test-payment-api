using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        [ForeignKey("VendedorId")]
        public int VendedorId { get; set; }
        public DateTime Data { get; set; }
        [Required]
        public String Itens { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}