using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public int Id { get; set; }
        [Required]
        public String CPF { get; set; }
        [Required]
        public String Nome { get; set; }
        [Required]
        public String Email { get; set; }
        [Required]
        public String Telefone { get; set; }
    }
}