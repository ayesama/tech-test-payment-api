using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            venda.Status = EnumStatusVenda.AguardandoPagamento;

            if (_context.Vendedores.Find(venda.VendedorId) == null || venda.Itens == "")
                return BadRequest();

            _context.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorID(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            var vendedor = _context.Vendedores.Find(venda.VendedorId);

            return Ok(new { venda, vendedor });
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarStatus(int id, EnumStatusVenda status)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            if ((venda.Status == EnumStatusVenda.AguardandoPagamento &&
                status == EnumStatusVenda.PagamentoAprovado) ||
                (venda.Status == EnumStatusVenda.AguardandoPagamento &&
                status == EnumStatusVenda.Cancelada) ||
                (venda.Status == EnumStatusVenda.PagamentoAprovado &&
                status == EnumStatusVenda.EnviadoParaTransportadora) ||
                (venda.Status == EnumStatusVenda.PagamentoAprovado &&
                status == EnumStatusVenda.Cancelada) ||
                (venda.Status == EnumStatusVenda.EnviadoParaTransportadora &&
                status == EnumStatusVenda.Entregue))
            {
                venda.Status = status;
                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(venda);
            }

            return BadRequest();
        }
    }
}
